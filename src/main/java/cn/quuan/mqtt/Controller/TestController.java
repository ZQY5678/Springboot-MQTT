package cn.quuan.mqtt.Controller;

import cn.quuan.mqtt.serve.MqttGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;

import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestController {
    @Autowired
    private MqttGateway mqttGateway;

    @GetMapping("/")
    public String test() {
        return "success";
    }

    //发送的主题为 hello
    @GetMapping("/sendMqtt")
    public String sendMqtt(String  sendData){
        mqttGateway.sendToMqtt(sendData,"hello");
        return "OK";
    }


}
